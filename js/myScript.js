function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function createMatrix(x, y) {
  var matrix = [];
  for (var i = 0; i < x; i++) {
    var row = [];
    for (var j = 0; j < y; j++) {
      row[j] = 0;
    }
    matrix[i] = row;
  }
  return matrix;
}

function drawPlane(state) {
  var plane = document.getElementsByClassName('plane')[0];
  for (var i = 0; i < state.matrix.length; i++) {
    var row = document.createElement('div');
    row.classList.add('row');
    for (var j = 0; j < state.matrix[0].length; j++) {
      var cell = document.createElement('div');
      cell.classList.add('cell');
      if (state.matrix[i][j] === 0) {
        cell.classList.add('field');
      } else if (state.matrix[i][j] === 1) {
        cell.classList.add('snake');
      } else if (state.matrix[i][j] === 2) {
        cell.classList.add('food');
      }
      row.appendChild(cell);
    }
    plane.appendChild(row);
  }
}

function applyChangesToPlane(state) {
  var plane = document.getElementsByClassName('plane')[0];
  var rows = plane.children;
  for (var i = 0; i < rows.length; i++) {
    var row = rows[i].children;
    for (var j = 0; j < row.length; j++) {
      if (row[j].classList.contains('field') && state.matrix[i][j] !== 0) {
        row[j].classList.remove('field');
        if (state.matrix[i][j] === 1) {
          row[j].classList.add('snake');
        } else if (state.matrix[i][j] === 2) {
          row[j].classList.add('food');
        }
      } else if (row[j].classList.contains('snake') && state.matrix[i][j] !== 1) {
        row[j].classList.remove('snake');
        if (state.matrix[i][j] === 0) {
          row[j].classList.add('field');
        } else if (state.matrix[i][j] === 2) {
          row[j].classList.add('food');
        }
      } else if (row[j].classList.contains('food') && state.matrix[i][j] !== 2) {
        row[j].classList.remove('food');
        if (state.matrix[i][j] === 0) {
          row[j].classList.add('field');
        } else if (state.matrix[i][j] === 1) {
          row[j].classList.add('snake');
        }
      }
    }
  }
}

function createFood(matrix) {
  while (true) {
    var x = getRandomIntInclusive(0, matrix.length - 1);
    var y = getRandomIntInclusive(0, matrix[0].length - 1);
    if (matrix[x][y] === 0) {
      matrix[x][y] = 2;
      break;
    }
  }
  return [x, y];
}

function createSnake(matrix) {
  while (true) {
    var x = getRandomIntInclusive(0, matrix.length - 1);
    var y = getRandomIntInclusive(0, matrix[0].length - 1);
    if (matrix[x][y] === 0) {
      matrix[x][y] = 1;
      break;
    }
  }
  return [[x, y]];
}

function moveSnake(state){
  state.matrix[state.snake[0][0]][state.snake[0][1]] = 0;
  if (state.snake[0][0] < state.foodLocation[0]){
    state.snake.unshift([state.snake[0][0] + 1, state.snake[0][1]]);
  } else if (state.snake[0][0] > state.foodLocation[0]) {
    state.snake.unshift([state.snake[0][0] - 1, state.snake[0][1]]);
  } else if (state.snake[0][1] < state.foodLocation[1]) {
    state.snake.unshift([state.snake[0][0], state.snake[0][1] + 1]);
  } else if (state.snake[0][1] > state.foodLocation[1]) {
    state.snake.unshift([state.snake[0][0], state.snake[0][1] - 1]);
  }
  if (state.snake[0][0] === state.foodLocation[0] &&
      state.snake[0][1] === state.foodLocation[1]) {
    state.shouldGrow = true;
    state.snake.push([[state.foodLocation[0]][state.foodLocation[1]]]);
    state.foodLocation = createFood(state.matrix);
  }
  if (!state.shouldGrow) {
    state.snake.pop();
  } else {
    state.shouldGrow = false;
  }

  for (var i = 0; i < state.snake.length; i++) {
    state.matrix[state.snake[i][0]][state.snake[i][1]] = 1;
  }
  applyChangesToPlane(state);
}

var state = {
  'matrix': createMatrix(50, 50),
  'shouldGrow': false
};
state.snake = createSnake(state.matrix);
state.foodLocation = createFood(state.matrix);
drawPlane(state);
setInterval(moveSnake, 20, state);
